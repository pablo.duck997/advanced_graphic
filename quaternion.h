#pragma once

#include <math.h>
#include "vector3.h"
#include "point3.h"
#include "versor3.h"

/* Quaternion class */
/* this class is a candidate to store a rotation! */
/* as such, it implements all expected methods    */

class Matrix3;
class AxisAngle;
class Euler;

class Quaternion{
public:

    /* fields */
    // TODO Q-Fields: which fields to store? (also add a constuctor taking these fields).
    Scalar x, y, z, w;

    Quaternion(Scalar a, Scalar b, Scalar c): x(a), y(b), z(c), w(1){} //done

    // TODO Q-Ide: this constructor construct the identity rotation
    Quaternion(): x(0), y(0), z(0), w(1) {} //done

    // TODO Q-FromPoint
    // returns a quaternion encoding a point
    Quaternion(const Point3& p): x(p.x), y(p.y), z(p.z), w(1) {} //done

    Vector3 apply( Vector3  v) const {
        // TODO Q-App: how to apply a rotation of this type?
        Quaternion point(v.asPoint());
        Quaternion Inverse = inverse();
        Quaternion result = *this * point * Inverse;
        return Vector3(result.x, result.y, result.z);
    } //done

    // Rotations can be applied to versors or vectors just as well
    Versor3 apply( Versor3 dir ) const {
        return apply( dir.asVector() ).asVersor();
    }

    Point3 apply( Point3 p ) const {
        return apply( p.asVector() ).asPoint();
    }

    // syntactic sugar: "R( p )" as a synomim of "R.apply( p )"
    Versor3 operator() (Versor3 p) { return apply(p); }
    Point3  operator() (Point3  p) { return apply(p); }
    Vector3 operator() (Vector3 p) { return apply(p); }

    Versor3 axisX() const {
    };  // TODO Q-Ax a
    Versor3 axisY() const {
    };  // TODO Q-Ax b
    Versor3 axisZ() const {
    };  // TODO Q-Ax c

    // conjugate
    Quaternion operator * (Quaternion r) const {
        /*
        qw= [w1w2 - x1x2 - y1y2 - z1z2 ]
        qx= [w1x2 + x1w2 + y1z2 - z1y2 ]
        qy= [w1y2 + y1w2 + z1x2 - x1z2 ]
        qz= [w1z2 + z1w2 + x1y2 - y1x2 ]
        */
        Quaternion q;

        q.x = w * r.x + x * r.w + y * r.z - z * r.y;
        q.y = w * r.y + y * r.w + z * r.x - x * r.z;
        q.z = w * r.z + z * r.w + x * r.y - y * r.x;
        q.w = w * r.w - z * r.x - y * r.y - z * r.z;

        return q;
    } //done

    Quaternion inverse() const{
        //TODO Q-Inv a  DONE
        Quaternion quaternion = *this;
        quaternion.invert();
        return quaternion;
    } //done

    void invert() {
        //q^-1 = q* / ||q|| 
        Scalar norm = sqrt(x * x + y * y + z * z + w * w);
        conjugate();
        x = x/norm;
        y = y/norm;
        z = z/norm;
        w = w/norm;
    } //done

    // specific methods for quaternions...
    Quaternion conjugated() const{
        //convert the immaginary part
        return Quaternion(-x, -y, -z);
    }

    void conjugate(){
        x = -x;
        y = -y;
        z = -z;
    } //done

    // returns a rotation to look toward target, if you are in eye, and the up-vector is up
    static Quaternion lookAt( Point3 eye, Point3 target, Versor3 up = Versor3::up() ){
        // TODO Q-LookAt
        return Quaternion();
    }

    // returns a rotation
    static Quaternion toFrom( Versor3 to, Versor3 from ){
        // TODO Q-ToFrom
        return Quaternion();
    }

    static Quaternion toFrom( Vector3 to, Vector3 from ){
        return toFrom( normalize(to) , normalize(from) );
    }

    // conversions to this representation
    static Quaternion from( Matrix3 m );   // TODO M2Q
    static Quaternion from( Euler e );     // TODO E2Q
    static Quaternion from( AxisAngle e ); // TODO A2Q

    // does this quaternion encode a rotation?
    bool isRot() const{
        // TODO Q-isR
        return false;
    }


    // does this quaternion encode a poont?
    bool isPoint() const{
        // TODO Q-isP
        if (w == 0)
            return true;

        return false;
    } //done

    void printf() const {
        std::cout << "Quaternion: x = " << x << " y = " << y << " z = " << z;
    } // TODO Print
};


// interpolation or roations
inline Quaternion lerp( const Quaternion& a,const Quaternion& b, Scalar t){
    // TODO Q-Lerp: how to interpolate quaternions
    // hints: shortest path! Also, consdider them are 4D unit vectors.
    Quaternion q;

    if (t > 1.0)
    {
        q = lerp(a, b, t - 1.0);
        return q * a.inverse() * b;
    }

    if (t < 0.0)
        return lerp(b, a, 1.0 - t);

    return lerp(a, b, t);
} //done



