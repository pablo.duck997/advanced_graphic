#include "versor3.h"
#include <iostream>

void Versor3::printf() const {
	std::cout << "Versor3: " << " x = " << Versor3::x <<
		" y = " << Versor3::y << " z = " << Versor3::z << std::endl;
}

Versor3 nlerp(const Versor3& a, const Versor3& b, Scalar t) {
	return normalize((1 - t) * a + t * b);
}

Versor3 slerp(const Versor3& a, const Versor3& b, Scalar t) {

    Scalar prod = dot(a, b);
    Scalar angle = acos(prod);

    Vector3 d1 = ((sin((1 - t) * angle)) / sin(angle)) * a;
    Vector3 d2 = ((sin(t * angle)) / sin(angle)) * b;
    Vector3 d = d1 + d2;

    return d.asVersor();
}