#include "transform.h"

Transform lerp(const Transform& a, const Transform& b, Scalar t) {
    Transform transform;

    transform.scale = ((1 - t) * a.scale) + t * b.scale;
    transform.rotation = lerp(a.rotation, b.rotation, t);
    transform.translation = lerp(a.translation, b.translation, t);

    return transform;
}